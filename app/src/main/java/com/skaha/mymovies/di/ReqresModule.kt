package com.skaha.mymovies.di

import com.skaha.mymovies.data.MovieDataStore
import com.skaha.mymovies.data.MovieRepository
import com.skaha.mymovies.ui.detail.DetailViewModel
import com.skaha.mymovies.ui.main.MainViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val reqresModule = module {
    single<MovieRepository> {
        MovieDataStore(
            get()
        )
    }
    viewModel { MainViewModel(get()) }
    viewModel { DetailViewModel(get()) }
}