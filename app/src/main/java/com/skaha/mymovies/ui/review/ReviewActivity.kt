package com.skaha.mymovies.ui.review

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import com.oratakashi.viewbinding.core.binding.activity.viewBinding
import com.oratakashi.viewbinding.core.tools.State
import com.skaha.mymovies.databinding.ActivityReviewBinding
import com.skaha.mymovies.databinding.AdapterToolbarBinding
import com.skaha.mymovies.ui.detail.DetailViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

class ReviewActivity : AppCompatActivity() {

    private val binding: ActivityReviewBinding by viewBinding()
    private lateinit var customToolbar: AdapterToolbarBinding
    private val viewModel: DetailViewModel by viewModel()
    private var data: Int? = null
    private val reviewAdapter: ReviewAdapter by lazy {
        ReviewAdapter()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        with(binding) {
            data = intent.getIntExtra("id", 0)
            customToolbar = binding.toolbar
            setSupportActionBar(customToolbar.containerBar)
            supportActionBar?.apply {
                setDisplayHomeAsUpEnabled(true)
            }
            initAction()
            initObserver()
        }
    }

    private fun initAction() {
        data.let {
            viewModel.getReview(it ?: 0)
        }
    }

    private fun initObserver() {
        with(binding) {
            viewModel.review.observe(this@ReviewActivity) {
                when (it) {
                    is State.Loading -> {}
                    is State.Success -> {
                        if (it.data.isNotEmpty()) {
                            rvReview.also {
                                val llm = LinearLayoutManager(this@ReviewActivity)
                                llm.orientation = LinearLayoutManager.VERTICAL
                                it.layoutManager = llm
                            }

                            rvReview.apply {
                                adapter = reviewAdapter
                                reviewAdapter.submitData(it.data)
                            }
                        }
                    }
                    else -> {}
                }
            }
        }
    }

}