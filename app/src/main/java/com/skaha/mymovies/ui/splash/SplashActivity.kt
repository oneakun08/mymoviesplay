package com.skaha.mymovies.ui.splash

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.oratakashi.viewbinding.core.binding.activity.viewBinding
import com.oratakashi.viewbinding.core.tools.startActivity
import com.skaha.mymovies.databinding.ActivitySplashBinding
import com.skaha.mymovies.ui.main.MainActivity

class SplashActivity : AppCompatActivity() {

    private val binding: ActivitySplashBinding by viewBinding()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        with(binding) {
            lottie.addAnimatorUpdateListener {
                if(it.animatedFraction == 1.0f){
                    startActivity(MainActivity::class.java)
                    finish()
                }
            }
        }
    }
}