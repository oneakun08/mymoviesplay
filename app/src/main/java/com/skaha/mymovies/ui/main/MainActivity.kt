package com.skaha.mymovies.ui.main

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.core.view.children
import com.google.android.material.tabs.TabLayoutMediator
import com.oratakashi.viewbinding.core.binding.activity.viewBinding
import com.oratakashi.viewbinding.core.tools.State
import com.oratakashi.viewbinding.core.tools.showDefaultLayout
import com.oratakashi.viewbinding.core.tools.showLoadingLayout
import com.oratakashi.viewbinding.core.tools.startActivity
import com.oratakashi.viewbinding.core.tools.toast
import com.skaha.mymovies.customview.CategoriesView
import com.skaha.mymovies.data.model.discover.DataDiscover
import com.skaha.mymovies.databinding.ActivityMainBinding
import com.skaha.mymovies.ui.detail.DetailActivity
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.viewModel

class MainActivity : AppCompatActivity() {

    private val binding: ActivityMainBinding by viewBinding()
    private val slider: SliderAdapter by lazy {
        SliderAdapter(this, dataSlider)
    }
    private val dataSlider: MutableList<DataDiscover> by lazy {
        ArrayList()
    }
    private val viewModel: MainViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        with(binding) {
            initAction()
            initObserver()

            vpSlider.adapter = slider
            TabLayoutMediator(tlHome, vpSlider) { _, _ -> }.attach()

        }
    }
    private fun initAction() {
        viewModel.getHome()
    }

    private fun initObserver() {
        with(binding) {
            viewModel.genre.observe(this@MainActivity) {
                when (it) {
                    is State.Loading -> {
                        toast("Loading")
                    }
                    is State.Success -> {
                        MainScope().launch {
                            it.data.forEach { data ->
                                val view = CategoriesView(
                                    applicationContext
                                )
                                    .setHeader(data.toGenre())
                                    .initData { genre ->
                                        if (genre != null) {
                                            viewModel.getGenre(genre.id)
                                        }
                                    }
                                containerContent.addView(view)
                                delay(200)
                            }
                        }
                    }
                    is State.Failure -> {
                        toast("Fail ${it.message}")
                        it.throwable?.printStackTrace()
                    }

                    else -> {}
                }
            }
            viewModel.discover.observe(this@MainActivity) {
                when (it) {
                    is State.Loading -> msvContent.showLoadingLayout()
                    is State.Success -> {
                        dataSlider.clear()
                        dataSlider.addAll(it.data)
                        msvContent.showDefaultLayout()
                        slider.notifyDataSetChanged()
                    }
                    is State.Failure -> {
                        toast("Fail ${it.message}")
                        it.throwable?.printStackTrace()
                    }

                    else -> {}
                }
            }
            viewModel.movie.observe(this@MainActivity) {
                when (it) {
                    is State.Success -> {
                        containerContent.children.findLast { view ->
                            view.tag == it.data.first.toString()
                        }?.let { view ->
                            (view as CategoriesView).submitData(it.data.second) { data ->
                                startActivity(DetailActivity::class.java) {
                                    it.putExtra("data", data)
                                }
                            }
                        }
                    }
                    is State.Failure -> {
                        it.throwable?.printStackTrace()
                        Log.e("debug", "debug: ${it.message}")
                    }

                    else -> {}
                }
            }
        }
    }
}