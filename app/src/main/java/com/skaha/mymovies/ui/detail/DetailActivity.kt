package com.skaha.mymovies.ui.detail

import android.content.res.Configuration
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import coil.load
import coil.transform.RoundedCornersTransformation
import com.oratakashi.viewbinding.core.binding.activity.viewBinding
import com.oratakashi.viewbinding.core.binding.intent.intent
import com.oratakashi.viewbinding.core.tools.State
import com.oratakashi.viewbinding.core.tools.invisible
import com.oratakashi.viewbinding.core.tools.onClick
import com.oratakashi.viewbinding.core.tools.showDefaultLayout
import com.oratakashi.viewbinding.core.tools.showLoadingLayout
import com.oratakashi.viewbinding.core.tools.startActivity
import com.oratakashi.viewbinding.core.tools.toast
import com.oratakashi.viewbinding.core.tools.visible
import com.skaha.mymovies.BuildConfig
import com.skaha.mymovies.data.model.discover.DataDiscover
import com.skaha.mymovies.data.model.trailer.DataTrailer
import com.skaha.mymovies.data.model.trailer.TrailerEntity
import com.skaha.mymovies.databinding.ActivityDetailBinding
import com.skaha.mymovies.ui.review.ReviewActivity
import com.skaha.mymovies.ui.trailer.TrailerActivity
import com.skaha.mymovies.utils.dateFormat
import org.koin.androidx.viewmodel.ext.android.viewModel

class DetailActivity : AppCompatActivity() {

    private val binding: ActivityDetailBinding by viewBinding()

    private val trailerAdapter: TrailerAdapter by lazy {
        TrailerAdapter()
    }

    private val reviewAdapter: ReviewAdapter by lazy {
        ReviewAdapter()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        with(binding) {
            setupToolbar()
            initObserver()
            initAction()
            setAction()
        }
    }

    private fun initObserver() {
        with(binding) {
            viewModel.movie.observe(this@DetailActivity) {
                when (it) {
                    is State.Loading -> msvContent.showLoadingLayout()
                    is State.Success -> {
                        msvContent.showDefaultLayout()
                        ivBackground.load(BuildConfig.IMAGE_URL + it.data.backdrop_path) {
                            crossfade(true)
//                            placeholder(R.drawable.placeholder_landscape)
                        }
                        ivImage.load(BuildConfig.IMAGE_URL + it.data.poster_path) {
                            crossfade(true)
//                            placeholder(R.drawable.placeholder_portrait)
                            transformations(RoundedCornersTransformation(25f))
                        }
                        tvTitle.text = it.data.title
                        if (it.data.release_date != null && it.data.release_date?.isNotEmpty() == true) {
                            tvDate.dateFormat(
                                it.data.release_date.orEmpty(),
                                "yyyy-MM-dd",
                                "dd MMMM yyyy"
                            )
                        } else {
                            tvDate.text = "Unknown"
                        }
                        tvOverview.text = if (it.data.overview!!.isNotEmpty()) {
                            it.data.overview
                        } else {
                            "Unknown"
                        }
                        if (it.data.vote_average != null) {
                            rbRatting.rating = ((it.data.vote_average ?: 0f) / 2)
                            tvRatting.text = ((it.data.vote_average ?: 0f) / 2).toString()
                        }

                        initReview(data.id)


                    }

                    else -> {}
                }
            }

            viewModel.trailer.observe(this@DetailActivity) {
                when (it) {
                    is State.Loading -> msvContent.showLoadingLayout()
                    is State.Success -> {
                        msvContent.showDefaultLayout()
                        rvVideo.also {
                            val llm = LinearLayoutManager(this@DetailActivity)
                            llm.orientation = LinearLayoutManager.HORIZONTAL
                            it.layoutManager = llm
                        }

                        rvVideo.apply {
                            adapter = trailerAdapter
                            trailerAdapter.apply {
                                submitData(it.data)
                                setAction { data ->
                                    startActivity(TrailerActivity::class.java) {
                                        it.putExtra("data_key", data.key)
                                        it.putExtra("data_name", data.name)
                                    }
                                }
                            }
                        }
                    }

                    else -> {}
                }
            }

            viewModel.review.observe(this@DetailActivity) {
                when (it) {
                    is State.Loading -> {}
                    is State.Success -> {
                        if (it.data.isNotEmpty()) {
                            msvContent.showDefaultLayout()
                            rvReview.also {
                                val llm = LinearLayoutManager(this@DetailActivity)
                                llm.orientation = LinearLayoutManager.VERTICAL
                                it.layoutManager = llm
                            }

                            rvReview.apply {
                                adapter = reviewAdapter
                                reviewAdapter.submitData(it.data)
                            }
                        }else{
                            tvTitleReview.invisible()
                            tvShowAll.invisible()
                        }
                    }

                    else -> {}
                }
            }
        }
    }

    private fun initAction() {
        viewModel.getDetailMovie(data.id)
    }

    private fun setAction(){
        with(binding){
            tvShowAll.setOnClickListener{
                startActivity(ReviewActivity::class.java){
                    it.putExtra("id", data.id)
                }
            }
        }
    }

    private fun initTrailer(id: Int) {
        viewModel.getTrailer(id)
    }

    private fun initReview(id: Int) {
        initTrailer(id)
        viewModel.getReview(id)
    }

    @Suppress("DEPRECATION")
    private fun setupToolbar() {
        with(binding) {
            ivClose.onClick { onBackPressed() }
            ivBack.onClick { onBackPressed() }

            tvToolbar.text = data.title
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.R) {
                when (resources.configuration.uiMode and Configuration.UI_MODE_NIGHT_MASK) {
                    Configuration.UI_MODE_NIGHT_YES -> {
                        window.decorView.systemUiVisibility =
                            (View.SYSTEM_UI_FLAG_LAYOUT_STABLE or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN)
                    }

                    Configuration.UI_MODE_NIGHT_NO -> {
                        window.decorView.systemUiVisibility =
                            (View.SYSTEM_UI_FLAG_LAYOUT_STABLE or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN or View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR)
                    }
                }
            } else {
                window.setDecorFitsSystemWindows(true)
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                nsContent.setOnScrollChangeListener { _, _, scrollY, _, _ ->
                    if (scrollY > 250) {
                        toolbar.visible()
                        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.R) {
                            when (resources.configuration.uiMode and Configuration.UI_MODE_NIGHT_MASK) {
                                Configuration.UI_MODE_NIGHT_YES -> {
                                    window.decorView.systemUiVisibility =
                                        View.SYSTEM_UI_FLAG_VISIBLE
                                }

                                Configuration.UI_MODE_NIGHT_NO -> {
                                    window.decorView.systemUiVisibility =
                                        (View.SYSTEM_UI_FLAG_VISIBLE or View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR)
                                }
                            }
                        } else {
                            window.setDecorFitsSystemWindows(true)
                        }
                    } else {
                        toolbar.invisible()
                        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.R) {
                            when (resources.configuration.uiMode and Configuration.UI_MODE_NIGHT_MASK) {
                                Configuration.UI_MODE_NIGHT_YES -> {
                                    window.decorView.systemUiVisibility =
                                        (View.SYSTEM_UI_FLAG_LAYOUT_STABLE or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN)
                                }

                                Configuration.UI_MODE_NIGHT_NO -> {
                                    window.decorView.systemUiVisibility =
                                        (View.SYSTEM_UI_FLAG_LAYOUT_STABLE or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN)
                                }
                            }
                        } else {
                            window.setDecorFitsSystemWindows(false)
                        }
                    }
                }
            }
        }
    }

    private val viewModel: DetailViewModel by viewModel()
    private val data: DataDiscover by intent("data")
}