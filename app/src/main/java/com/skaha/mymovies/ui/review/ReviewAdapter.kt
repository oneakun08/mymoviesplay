package com.skaha.mymovies.ui.review

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.oratakashi.viewbinding.core.binding.recyclerview.ViewHolder
import com.oratakashi.viewbinding.core.binding.recyclerview.viewBinding
import com.oratakashi.viewbinding.core.tools.onClick
import com.skaha.mymovies.data.model.review.DataReview
import com.skaha.mymovies.databinding.AdapterReviewBinding
import com.skaha.mymovies.utils.loadImage

class ReviewAdapter(
    private var onClick: ((DataReview) -> Unit)? = null
) : RecyclerView.Adapter<ViewHolder<AdapterReviewBinding>>() {
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder<AdapterReviewBinding> = viewBinding(parent)

    override fun onBindViewHolder(holder: ViewHolder<AdapterReviewBinding>, position: Int) {
        with(holder.binding) {

            holder.binding.tvName.text = data[position].authorDetails?.username
            holder.binding.tvContent.text = data[position].content

            loadImage(
                holder.binding.ivImage, "https://image.tmdb.org/t/p/w185" + data[position].authorDetails?.avatarPath
            )

            root.onClick { onClick?.invoke(data[position]) }
        }
    }

    override fun getItemCount(): Int {
        return data.size
    }

    fun submitData(data: List<DataReview>) {
        this.data.clear()
        this.data.addAll(data)
        notifyDataSetChanged()
    }

    fun setAction(action: ((DataReview) -> Unit)?) {
        onClick = action
    }

    private val data: MutableList<DataReview> by lazy {
        ArrayList()
    }
}