package com.skaha.mymovies.ui.detail

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.oratakashi.viewbinding.core.binding.recyclerview.ViewHolder
import com.oratakashi.viewbinding.core.binding.recyclerview.viewBinding
import com.oratakashi.viewbinding.core.tools.loadImage
import com.oratakashi.viewbinding.core.tools.onClick
import com.skaha.mymovies.BuildConfig
import com.skaha.mymovies.data.model.discover.DataDiscover
import com.skaha.mymovies.data.model.trailer.DataTrailer
import com.skaha.mymovies.data.model.trailer.TrailerEntity
import com.skaha.mymovies.databinding.AdapterHomeBinding
import com.skaha.mymovies.databinding.AdapterTrailerBinding
import com.skaha.mymovies.utils.loadImage

class TrailerAdapter(
    private var onClick: ((TrailerEntity) -> Unit)? = null
) : RecyclerView.Adapter<ViewHolder<AdapterTrailerBinding>>() {
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder<AdapterTrailerBinding> = viewBinding(parent)

    override fun onBindViewHolder(holder: ViewHolder<AdapterTrailerBinding>, position: Int) {
        with(holder.binding) {
//            ivImage.load(BuildConfig.IMAGE_URL + data[position].poster_path) {
//                crossfade(true)
//                placeholder(R.drawable.placeholder_portrait)
//            }
//            ivImage.loadImage(
//                root.context,
////                imageUrl = BuildConfig.IMAGE_URL + data[position].key,
//                imageUrl = "http://img.youtube.com/vi/" + data[position].key + "/0.jpg",
//                //placeHolderResourceId = R.drawable.placeholder_portrait
//            )

            loadImage(
                holder.binding.ivImage, "http://img.youtube.com/vi/" + data[position].key + "/0.jpg"
            )

            root.onClick { onClick?.invoke(data[position]) }
        }
    }

    override fun getItemCount(): Int {
        return data.size
    }

    fun submitData(data: List<TrailerEntity>) {
        this.data.clear()
        this.data.addAll(data)
        notifyDataSetChanged()
    }

    fun setAction(action: ((TrailerEntity) -> Unit)?) {
        onClick = action
    }

    private val data: MutableList<TrailerEntity> by lazy {
        ArrayList()
    }
}