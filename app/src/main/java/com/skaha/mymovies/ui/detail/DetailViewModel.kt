package com.skaha.mymovies.ui.detail

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.oratakashi.viewbinding.core.binding.livedata.liveData
import com.oratakashi.viewbinding.core.tools.State
import com.skaha.mymovies.data.MovieRepository
import com.skaha.mymovies.data.model.movie.ResponseMovie
import com.skaha.mymovies.data.model.review.DataReview
import com.skaha.mymovies.data.model.trailer.DataTrailer
import com.skaha.mymovies.data.model.trailer.TrailerEntity
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch

class DetailViewModel(
    private val repo: MovieRepository
) : ViewModel() {
    private val job: Job by lazy { Job() }

    private val _movie: MutableLiveData<State<ResponseMovie>> by liveData(State.default())
    val movie: LiveData<State<ResponseMovie>> = _movie

    private val _trailer: MutableLiveData<State<List<TrailerEntity>>> by liveData(State.default())
    val trailer: LiveData<State<List<TrailerEntity>>> = _trailer

    private val _review: MutableLiveData<State<List<DataReview>>> by liveData(State.default())
    val review: LiveData<State<List<DataReview>>> = _review

    fun getDetailMovie(id: Int) {
        viewModelScope.launch {
            repo.getDetailMovie(id)
                .onStart { _movie.postValue(State.loading()) }
                .catch { _movie.postValue(State.fail(it, it.message)) }
                .collect {
                    _movie.postValue(State.success(it))
                }
        }
    }

    fun getTrailer(id: Int) {
        viewModelScope.launch {
            repo.getTrailer(id)
                .onStart { _trailer.postValue(State.loading()) }
                .catch { _trailer.postValue(State.fail(it, it.message)) }
                .collect {
                    _trailer.postValue(State.success(it))
                }
        }
    }

    fun getReview(id: Int) {
        viewModelScope.launch {
            repo.getReview(id)
                .onStart { _review.postValue(State.loading()) }
                .catch { _review.postValue(State.fail(it, it.message)) }
                .collect {
                    _review.postValue(State.success(it))
                }
        }
    }

    override fun onCleared() {
        super.onCleared()
        job.cancel()
    }
}