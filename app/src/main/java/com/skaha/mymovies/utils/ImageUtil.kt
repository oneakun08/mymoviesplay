package com.skaha.mymovies.utils

import android.widget.ImageView
import com.bumptech.glide.Glide
import com.skaha.mymovies.R

fun loadImage(imageView: ImageView, url: String) {
    url.let {
        Glide.with(imageView)
            .load(url)
            .placeholder(R.drawable.ic_cinema_alt_svgrepo_com)
            .error(R.drawable.ic_cinema_alt_svgrepo_com)
            .into(imageView)
    }
}