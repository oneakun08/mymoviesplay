package com.skaha.mymovies.data.model.genre


data class GenreEntity(
    val id: Int,
    val name: String
) {
    fun toGenre(): DataGenre {
        return DataGenre( id, name)
    }
}
