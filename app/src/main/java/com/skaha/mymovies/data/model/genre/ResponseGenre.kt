package com.skaha.mymovies.data.model.genre

import com.google.gson.annotations.SerializedName
import com.skaha.mymovies.data.model.genre.DataGenre

data class ResponseGenre(
    @field:SerializedName("genres") val genres : List<DataGenre>,
)
