package com.skaha.mymovies.data.model.review

import com.google.gson.annotations.SerializedName
import com.skaha.mymovies.data.model.discover.DataDiscover

class ResponseReview (
    @SerializedName("results") val data : List<DataReview>?,
    @SerializedName("page") val page : Int?,
    @SerializedName("total_pages") val total_pages : Int?
)