package com.skaha.mymovies.data.model.review

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import com.skaha.mymovies.data.model.trailer.TrailerEntity
import kotlinx.android.parcel.Parcelize

@Parcelize
data class DataReview (
    @SerializedName("author_details")
    val authorDetails: DataAuthorReview? = null,

    @SerializedName("updated_at")
    val updatedAt: String? = null,

    @SerializedName("author")
    val author: String? = null,

    @SerializedName("created_at")
    val createdAt: String? = null,

    @SerializedName("id")
    val id: String? = null,

    @SerializedName("content")
    val content: String? = null,

    @SerializedName("url")
    val url: String? = null
): Parcelable {
    fun toReviewEntity() : ReviewEntity {
        return ReviewEntity( id, author, content, url)
    }
}