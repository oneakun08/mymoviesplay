package com.skaha.mymovies.data.model.trailer


data class TrailerEntity(
    val id: String,
    val name: String,
    val key: String
) {
    fun toTrailer(): DataTrailer {
        return DataTrailer(id = id, name = name, key = key, site = "YouTube", size = 1080, iso31661 = "US", official = true, type = "Trailer", publishedAt = "2021-08-13T00:00:00.000Z", iso6391 = "en")
    }
}
