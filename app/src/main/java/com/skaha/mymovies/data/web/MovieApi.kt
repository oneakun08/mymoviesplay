package com.skaha.mymovies.data.web

import com.skaha.mymovies.data.model.discover.ResponseDiscover
import com.skaha.mymovies.data.model.genre.ResponseGenre
import com.skaha.mymovies.data.model.movie.ResponseMovie
import com.skaha.mymovies.data.model.review.ResponseReview
import com.skaha.mymovies.data.model.trailer.TrailerResponse

class MovieApi(
    private val api: MovieApiClient
): MovieApiClient {
    override suspend fun getGenre(): ResponseGenre {
        return api.getGenre()
    }

    override suspend fun getDiscover(
        include_adult: Boolean,
        page: Int,
        sortBy: String
    ): ResponseDiscover {
        return api.getDiscover()
    }

    override suspend fun getDiscover(genre: Int, page: Int, sortBy: String): ResponseDiscover {
        return api.getDiscover(genre)
    }

    override suspend fun getDetailMovie(id: Int): ResponseMovie {
        return api.getDetailMovie(id)
    }

    override suspend fun getTrailer(id: Int): TrailerResponse {
        return api.getTrailer(id)
    }

    override suspend fun getReview(id: Int): ResponseReview {
        return api.getReview(id)
    }
}