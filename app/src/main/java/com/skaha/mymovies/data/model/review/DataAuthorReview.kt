package com.skaha.mymovies.data.model.review

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize


@Parcelize
data class DataAuthorReview (
    @SerializedName("avatar_path")
    val avatarPath: String? = null,

    @SerializedName("name")
    val name: String? = null,

    @SerializedName("rating")
    val rating: Double? = null,

    @SerializedName("username")
    val username: String? = null
) : Parcelable