package com.skaha.mymovies.data

import com.skaha.mymovies.data.model.discover.DataDiscover
import com.skaha.mymovies.data.model.genre.GenreEntity
import com.skaha.mymovies.data.model.movie.ResponseMovie
import com.skaha.mymovies.data.model.review.DataReview
import com.skaha.mymovies.data.model.trailer.DataTrailer
import com.skaha.mymovies.data.model.trailer.TrailerEntity
import kotlinx.coroutines.flow.Flow

interface MovieRepository {
    fun getGenre(): Flow<List<GenreEntity>>

    fun getDiscover(): Flow<List<DataDiscover>>

    fun getDiscover(genre: Int): Flow<List<DataDiscover>>

    fun getDetailMovie(id: Int): Flow<ResponseMovie>

    fun getReview(id: Int): Flow<List<DataReview>>

    fun getTrailer(id: Int): Flow<List<TrailerEntity>>

}