package com.skaha.mymovies.data.model.trailer

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class DataTrailer (
    @SerializedName("site")
    val site: String,

    @SerializedName("size")
    val size: Int,

    @SerializedName("iso_3166_1")
    val iso31661: String,

    @SerializedName("name")
    val name: String,

    @SerializedName("official")
    val official: Boolean,

    @SerializedName("id")
    val id: String,

    @SerializedName("type")
    val type: String,

    @SerializedName("published_at")
    val publishedAt: String,

    @SerializedName("iso_639_1")
    val iso6391: String,

    @SerializedName("key")
    val key: String
): Parcelable {
    fun toTrailerEntity() : TrailerEntity {
        return TrailerEntity( id, name, key)
    }
}