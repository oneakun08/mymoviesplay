package com.skaha.mymovies.data

import android.util.Log
import com.skaha.mymovies.data.model.discover.DataDiscover
import com.skaha.mymovies.data.model.genre.GenreEntity
import com.skaha.mymovies.data.model.movie.ResponseMovie
import com.skaha.mymovies.data.model.review.DataReview
import com.skaha.mymovies.data.model.review.ResponseReview
import com.skaha.mymovies.data.model.trailer.DataTrailer
import com.skaha.mymovies.data.model.trailer.TrailerEntity
import com.skaha.mymovies.data.web.MovieApi
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.flow.*

class MovieDataStore(
    private val webService: MovieApi,
) : MovieRepository {
    @OptIn(FlowPreview::class)
    override fun getGenre(): Flow<List<GenreEntity>> {
        return flow {
            val genre = webService.getGenre()
            emit(genre.genres.map { data -> data.toGenreEntity() })
        }.catch {
            Log.e("MovieDataStore", "getGenre: ${it.message}")
        }.flowOn(Dispatchers.IO)
    }

    @OptIn(FlowPreview::class)
    override fun getDiscover(): Flow<List<DataDiscover>> {
        return flow {
            val discover = webService.getDiscover()
            discover.data?.let { emit(it.subList(0, 5)) }
        }
            .catch {
                Log.e("MovieDataStore", "getDiscover: ${it.message}")
            }
            .flowOn(Dispatchers.IO)
    }

    @OptIn(FlowPreview::class)
    override fun getDiscover(genre: Int): Flow<List<DataDiscover>> {
        return flow {
            val discover = webService.getDiscover(genre)
            discover.data?.let { emit(it) }
        }.catch {
            Log.e("MovieDataStore", "getDiscover: ${it.message}")
        }
            .flowOn(Dispatchers.IO)
    }

    override fun getDetailMovie(id: Int): Flow<ResponseMovie> {
        return flow {
            val movie = webService.getDetailMovie(id)
            emit(movie)
        }.catch {
            Log.e("MovieDataStore", "getDetailMovie: ${it.message}")
        }
            .flowOn(Dispatchers.IO)
    }

    override fun getReview(id: Int): Flow<List<DataReview>> {
        return flow {
            val review = webService.getReview(id)
            review.data?.let {
                emit(it)
            }
        }.catch {
            Log.e("MovieDataStore", "getReview: ${it.message}")
        }.flowOn(Dispatchers.IO)
    }

    override fun getTrailer(id: Int): Flow<List<TrailerEntity>> {
        return flow {
            val trailer = webService.getTrailer(id)
            emit(trailer.results.map { data -> data.toTrailerEntity() })
        }.catch {
            Log.e("MovieDataStore", "getTrailer: ${it.message}")
        }.flowOn(Dispatchers.IO)
    }
}