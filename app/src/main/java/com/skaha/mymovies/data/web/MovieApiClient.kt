package com.skaha.mymovies.data.web

import com.skaha.mymovies.data.model.discover.ResponseDiscover
import com.skaha.mymovies.data.model.genre.ResponseGenre
import com.skaha.mymovies.data.model.movie.ResponseMovie
import com.skaha.mymovies.data.model.review.ResponseReview
import com.skaha.mymovies.data.model.trailer.TrailerResponse
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Path
import retrofit2.http.Query

interface MovieApiClient {
    @GET("genre/movie/list")
    suspend fun getGenre(): ResponseGenre

    @GET("discover/movie")
    suspend fun getDiscover(
        @Query("include_adult") include_adult: Boolean = false,
        @Query("page") page: Int = 1,
        @Query("sort_by") sortBy: String = "popularity.desc"
    ): ResponseDiscover

    @GET("discover/movie")
    suspend fun getDiscover(
        @Query("with_genres") genre: Int,
        @Query("page") page: Int = 1,
        @Query("sort_by") sortBy: String = "popularity.desc"
    ): ResponseDiscover

    @GET("movie/{id}")
    suspend fun getDetailMovie(
        @Path("id") id: Int
    ): ResponseMovie

    @GET("movie/{id}/videos")
    suspend fun getTrailer(
        @Path("id") id: Int
    ): TrailerResponse

    @GET("movie/{id}/reviews")
    suspend fun getReview(
        @Path("id") id: Int
    ): ResponseReview
}