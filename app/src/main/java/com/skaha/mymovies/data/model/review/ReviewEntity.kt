package com.skaha.mymovies.data.model.review

class ReviewEntity (
    val id: String?,
    val author: String?,
    val content: String?,
    val url: String?
) {
    fun toReviewEntity() : ReviewEntity {
        return ReviewEntity( id, author, content, url)
    }
}