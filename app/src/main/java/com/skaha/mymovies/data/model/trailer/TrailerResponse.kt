package com.skaha.mymovies.data.model.trailer

import com.google.gson.annotations.SerializedName

class TrailerResponse (
    @SerializedName("id")
    val id: Int? = null,

    @SerializedName("results")
    val results: List<DataTrailer>
)